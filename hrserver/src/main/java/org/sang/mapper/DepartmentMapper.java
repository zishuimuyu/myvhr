package org.sang.mapper;

import org.apache.ibatis.annotations.Param;
import org.sang.bean.Department;

import java.util.List;

/**
 * Created by sang on 2018/1/7.
 */
public interface DepartmentMapper {
    /**
     * 新增部门
     * @param department
     */
    void addDep(@Param("dep") Department department);

    /**
     * 删除部门
     * @param department
     */
    void deleteDep(@Param("dep") Department department);

    /**
     * 根据上级部门id查找这个上级部门的下级部门
     * @param pid 父部门id
     * @return
     */
    List<Department> getDepByPid(Long pid);

    /**
     * 获取所有部门信息
     * @return
     */
    List<Department> getAllDeps();
}
