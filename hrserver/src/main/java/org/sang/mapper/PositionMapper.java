package org.sang.mapper;

import org.apache.ibatis.annotations.Param;
import org.sang.bean.Position;

import java.util.List;

/**
 * Created by sang on 2018/1/10.
 */
public interface PositionMapper {

    int addPos(@Param("pos") Position pos);

    Position getPosByName(String name);
    /**
     * 获取所有职位信息
     * @return
     */
    List<Position> getAllPos();

    int deletePosById(@Param("pids") String[] pids);
    /**
     * 更新/修改职位信息
     * @param position 职位实体
     * @return
     */
    int updatePosById(@Param("pos") Position position);
}
