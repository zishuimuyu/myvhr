package org.sang.mapper;

import org.apache.ibatis.annotations.Param;
import org.sang.bean.Hr;
import org.sang.bean.Role;

import java.util.List;

/**
 * Created by sang on 2017/12/28.
 */
public interface HrMapper {
    Hr loadUserByUsername(String username);

    List<Role> getRolesByHrId(Long id);

    /**
     * HR/管理员注册
     *
     * @param username 用户名
     * @param password 密码
     * @return
     */
    int hrReg(@Param("username") String username, @Param("password") String password);

    /**
     * 根据搜索关键字查找HR
     *
     * @param keywords 关键字
     * @return
     */
    List<Hr> getHrsByKeywords(@Param("keywords") String keywords);

    /**
     * 更新Hr的信息
     *
     * @param hr hr实体
     * @return
     */
    int updateHr(Hr hr);

    /**
     * 删除HR的角色信息
     *
     * @param hrId HRid
     * @return
     */
    int deleteRoleByHrId(Long hrId);

    /**
     * 新增HR的角色信息
     *
     * @param hrId HRid
     * @param rids 角色id(多个角色用","拼接)
     * @return
     */
    int addRolesForHr(@Param("hrId") Long hrId, @Param("rids") Long[] rids);

    /**
     * 根据HRid获取Hr的信息
     *
     * @param hrId HRid
     * @return
     */
    Hr getHrById(Long hrId);

    /**
     * 根据HRid删除HR
     *
     * @param hrId Hrid
     * @return
     */
    int deleteHr(Long hrId);
    /**
     * 获取所有的HR信息(不包括管理员)
     * @return
     */
    List<Hr> getAllHr(@Param("currentId") Long currentId);
}
