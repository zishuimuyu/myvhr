package org.sang.mapper;

import org.apache.ibatis.annotations.Param;
import org.sang.bean.Role;

import java.util.List;

/**
 * Created by sang on 2018/1/1.
 */
public interface RoleMapper {
    /**
     * 查找角色列表
     *
     * @return
     */
    List<Role> roles();

    /**
     * 新增角色
     *
     * @param role   角色实体
     * @param roleZh 角色中文名称
     * @return
     */
    int addNewRole(@Param("role") String role, @Param("roleZh") String roleZh);

    /**
     * 根据角色id删除角色
     *
     * @param rid 角色id
     * @return
     */
    int deleteRoleById(Long rid);
}
