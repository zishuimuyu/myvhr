package org.sang.mapper;

import org.apache.ibatis.annotations.Param;
import org.sang.bean.JobLevel;

import java.util.List;

/**
 * Created by sang on 2018/1/11.
 */
public interface JobLevelMapper {
    JobLevel getJobLevelByName(String name);
    /**
     * 新增职称信息
     * @return
     */
    int addJobLevel(@Param("jobLevel") JobLevel jobLevel);
    /**
     * 获取所有职称信息
     * @return
     */
    List<JobLevel> getAllJobLevels();
    /**
     * 根据职称id删除职称(可以删除多个),id以","拼接
     * @param ids 职称id
     * @return
     */
    int deleteJobLevelById(@Param("ids") String[] ids);
    /**
     * 修改/更新职称信息
     * @param jobLevel 职称实体
     * @return
     */
    int updateJobLevel(@Param("jobLevel") JobLevel jobLevel);
}
