package org.sang.mapper;

import org.apache.ibatis.annotations.Param;

/**
 * Created by sang on 2018/1/2.
 */
public interface MenuRoleMapper {

    /**
     * 根据角色id删除菜单
     * @param rid 角色id
     * @return
     */
    int deleteMenuByRid(@Param("rid") Long rid);

    /**
     * 新增角色菜单
     * @param rid 角色id
     * @param mids 菜单id(可以是多个)
     * @return
     */
    int addMenu(@Param("rid") Long rid, @Param("mids") Long[] mids);
}
