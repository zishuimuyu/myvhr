package org.sang.bean;

import java.util.Date;

/**
 * 消息
 *
 * @author：GJH
 * @createDate：2019/10/31
 * @company：洪荒宇宙加力蹲大学
 */
public class MsgContent {
    /**
     * id
     */
    private Long id;
    /**
     * 消息内容
     */
    private String message;
    /**
     * 消息标题
     */
    private String title;
    /**
     * 创建时间
     */
    private Date createDate;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }
}
