package org.sang.bean;

import java.sql.Timestamp;

/**
 * 职称
 *
 * @author：GJH
 * @createDate：2019/10/31
 * @company：洪荒宇宙加力蹲大学
 */
public class JobLevel {
    /**
     * 职称id
     */
    private Long id;
    /**
     * 职称名称
     */
    private String name;
    /**
     * 职称等级('正高级','副高级','中级','初级','员级')
     */
    private String titleLevel;
    /**
     * 创建时间
     */
    private Timestamp createDate;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        JobLevel jobLevel = (JobLevel) o;

        return name != null ? name.equals(jobLevel.name) : jobLevel.name == null;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }

    public JobLevel() {

    }

    public JobLevel(String name) {

        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitleLevel() {
        return titleLevel;
    }

    public void setTitleLevel(String titleLevel) {
        this.titleLevel = titleLevel;
    }

    public Timestamp getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
    }
}
