package org.sang.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 员工培训信息表
 *
 * @author：GJH
 * @createDate：2019/11/4
 * @company：洪荒宇宙加力蹲大学
 */
@Data
@NoArgsConstructor
public class EmployeeTrain {
    /**
     * id
     */
    private Long id;
    /**
     * 员工编号
     */
    private Long eid;
    /**
     * 培训日期
     */
    private Date trainDate;
    /**
     * 培训内容
     */
    private String trainContent;
    /**
     * 备注
     */
    private String remark;
}
