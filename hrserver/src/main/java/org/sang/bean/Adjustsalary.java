package org.sang.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 员工调薪表
 *
 * @author：GJH
 * @createDate：2019/11/4
 * @company：洪荒宇宙加力蹲大学
 */
@Data
@NoArgsConstructor
public class Adjustsalary {
    /**
     * id
     */
    private Long id;
    /**
     * 员工编号
     */
    private Long eid;
    /**
     * 调薪日期
     */
    private Date asDate;
    /**
     * 调前薪资
     */
    private Long beforeSalary;
    /**
     * 调后薪资
     */
    private Long afterSalary;
    /**
     * 调薪原因
     */
    private String reason;
    /**
     * 备注
     */
    private String remark;
}
