package org.sang.bean;

import lombok.Data;

import java.io.Serializable;

/**
 * 角色
 *
 * @author：GJH
 * @createDate：2019/10/31
 * @company：洪荒宇宙加力蹲大学
 */
@Data
public class Role implements Serializable {
    /**
     * 角色id
     */
    private Long id;
    /**
     * 角色名称
     */
    private String name;
    /**
     * 角色名称(中文)
     */
    private String nameZh;


}
