package org.sang.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 员工考评表
 *
 * @author：GJH
 * @createDate：2019/11/4
 * @company：洪荒宇宙加力蹲大学
 */
@Data
@NoArgsConstructor
public class Appraise {
    /**
     * id
     */
    private Long id;
    /**
     * 员工编号
     */
    private Long eid;
    /**
     * 考评日期
     */
    private Date appDate;

    /**
     * 考评内容
     */
    private String appContent;
    /**
     * 考评结果
     */
    private String appResult;
    /**
     * 备注
     */
    private String remark;
}
