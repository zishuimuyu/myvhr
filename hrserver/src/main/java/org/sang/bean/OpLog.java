package org.sang.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 操作日志表
 *
 * @author：GJH
 * @createDate：2019/11/4
 * @company：洪荒宇宙加力蹲大学
 */
@Data
@NoArgsConstructor
public class OpLog {
    /**
     * id
     */
    private Long id;

    /**
     * 添加日期(即操作发生的时间)
     */
    private Date addDate;

    /**
     * 操作内容
     */
    private String operate;

    /**
     * 操作员编号
     */
    private Long hrid;
}
