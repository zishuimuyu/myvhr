package org.sang.bean;

import lombok.Data;

import java.util.Date;

/**
 * 员工薪资表
 *
 * @author：GJH
 * @createDate：2019/10/31
 * @company：洪荒宇宙加力蹲大学
 */
@Data
public class Salary {
    /**
     * id
     */
    private Integer id;
    /**
     * 基本工资
     */
    private Integer basicSalary;
    /**
     * 奖金
     */
    private Integer bonus;
    /**
     * 午餐补助
     */
    private Integer lunchSalary;
    /**
     * 交通补助
     */
    private Integer trafficSalary;
    /**
     * 应发工资
     */
    private Integer allSalary;
    /**
     * 养老金基数
     */
    private Integer pensionBase;
    /**
     * 养老金比率
     */
    private Float pensionPer;

    /**
     * 医疗基数
     */
    private Integer medicalBase;
    /**
     * 医疗保险比率
     */
    private Float medicalPer;
    /**
     * 公积金基数
     */
    private Integer accumulationFundBase;
    /**
     * 公积金比率
     */
    private Float accumulationFundPer;
    /**
     * 名称
     */
    private String name;
    /**
     * 启用时间
     */
    private Date createDate;


}