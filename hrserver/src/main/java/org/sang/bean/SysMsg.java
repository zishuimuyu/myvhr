package org.sang.bean;

import lombok.Data;

/**
 * 系统消息
 *
 * @author：GJH 113  25  31 。奖罚
 * @createDate：2019/10/31
 * @company：洪荒宇宙加力蹲大学
 */
@Data
public class SysMsg {
    /**
     * id
     */
    private Long id;
    /**
     * 消息id
     */
    private Long mid;
    /**
     * 消息类型(0表示群发消息)
     */
    private Integer type;
    /**
     * 这条消息是发给谁的(接收人id)
     */
    private Long hrid;
    /**
     * 消息状态(0未读1已读)
     */
    private Integer state;
    /**
     * 消息内容
     */
    private MsgContent msgContent;

}
