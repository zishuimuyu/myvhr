package org.sang.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 员工奖惩信息
 *
 * @author：GJH
 * @createDate：2019/11/4
 * @company：洪荒宇宙加力蹲大学
 */
@Data
@NoArgsConstructor
public class EmployeeEc {
    /**
     * id
     */
    private Long id;
    /**
     * 员工编号
     */
    private Long eid;
    /**
     * 奖罚日期
     */
    private Date ecDate;
    /**
     * 奖罚原因
     */
    private String ecReason;
    /**
     * 奖罚分
     */
    private Long ecPoint;
    /**
     * 奖罚类别，0：奖，1：罚
     */
    private Long ecType;
    /**
     * 备注
     */
    private String remark;
}
