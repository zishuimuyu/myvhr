package org.sang.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 员工调动表
 *
 * @author：GJH
 * @createDate：2019/11/4
 * @company：洪荒宇宙加力蹲大学
 */
@Data
@NoArgsConstructor
public class EmployeeRemove {
    /**
     * id
     */
    private Long id;
    /**
     * 员工编号
     */
    private Long eid;
    /**
     * 调动后部门
     */
    private Long afterDepId;
    /**
     * 调动后职位
     */
    private Long afterJobId;
    /**
     * 调动日期
     */
    private Date removeDate;
    /**
     * 调动原因
     */
    private String reason;

    /**
     * 备注
     */
    private String remark;
}
