package org.sang.bean;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 地区名称
 *
 * @author：GJH
 * @createDate：2020-04-02
 * @company：北京木联能软件股份有限公司
 */
@Data
@NoArgsConstructor

public class Region {
    /**
     * ID
     */
    private Integer id;
    /**
     * 父ID
     */
    private Integer pid;
    /**
     * 路径
     */
    private String path;
    /**
     * 中文名称
     */
    private String chName;
    /**
     * 英文名陈
     */
    private String enName;
    /**
     * 邮政编码
     */
    private String postCode;
    /**
     * 电话区号
     */
    private String areaCode;
    /**
     * 行政代码
     */
    private String adminCode;

    /**
     * 排序
     */
    private String sortNumber;

    /**
     * 层级
     */
    private Integer level;

    /**
     * 纬度
     */
    private String lat;

    /**
     * 经度
     */
    private String lng;

    /**
     * 状态值 0:无效 1:有效
     */
    private int status;
}
