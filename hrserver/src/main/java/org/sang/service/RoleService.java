package org.sang.service;

import org.sang.bean.Role;
import org.sang.mapper.RoleMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sang on 2018/1/1.
 */
@Service
@Transactional
public class RoleService {
    @Autowired
    RoleMapper roleMapper;

    /**
     * 查找角色列表
     * @return
     */
    public List<Role> roles() {
        return roleMapper.roles();
    }
    /**
     * 新增角色
     * @param role 角色实体
     * @param roleZh 角色中文名称
     * @return
     */
    public int addNewRole(String role, String roleZh) {
        if (!role.startsWith("ROLE_")) {
            role = "ROLE_" + role;
        }
        return roleMapper.addNewRole(role, roleZh);
    }
    /**
     * 根据角色id删除角色
     * @param rid 角色id
     * @return
     */
    public int deleteRoleById(Long rid) {
        return roleMapper.deleteRoleById(rid);
    }
}
