package org.sang.service;

import org.sang.bean.Department;
import org.sang.mapper.DepartmentMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * 部门服务类
 *
 * @author：GJH
 * @createDate：2019/10/31
 * @company：洪荒宇宙加力蹲大学
 */
@Service
@Transactional
public class DepartmentService {
    @Autowired
    DepartmentMapper departmentMapper;
    /**
     * 新增部门信息
     * @param department
     * @return
     */
    public int addDep(Department department) {
        department.setEnabled(true);
        departmentMapper.addDep(department);
        return department.getResult();
    }

    /**
     * 删除部门信息
     * @param did 部门id
     * @return
     */
    public int deleteDep(Long did) {
        Department department = new Department();
        department.setId(did);
        departmentMapper.deleteDep(department);
        return department.getResult();
    }
    /**
     * 根据上级部门id查找这个上级部门的下级部门
     * @param pid 父部门id
     * @return
     */
    public List<Department> getDepByPid(Long pid) {
        return departmentMapper.getDepByPid(pid);
    }
    /**
     * 获取所有部门信息
     * @return
     */
    public List<Department> getAllDeps() {
        return departmentMapper.getAllDeps();
    }
}
