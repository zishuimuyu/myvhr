package org.sang.service;

import org.sang.bean.Position;
import org.sang.mapper.PositionMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sang on 2018/1/10.
 */
@Service
@Transactional
public class PositionService {
    @Autowired
    PositionMapper positionMapper;

    /**
     * 新增职位
     *
     * @param pos 职位信息
     * @return
     */
    public int addPos(Position pos) {
        if (positionMapper.getPosByName(pos.getName()) != null) {
            return -1;
        }
        return positionMapper.addPos(pos);
    }

    /**
     * 获取所有职位信息
     *
     * @return List<Position>
     */
    public List<Position> getAllPos() {
        return positionMapper.getAllPos();
    }

    /**
     * 根据职位id删除职位信息
     * @param pids 职位id,多个 以","拼接
     * @return boolean
     */
    public boolean deletePosById(String pids) {
        String[] split = pids.split(",");
        return positionMapper.deletePosById(split) == split.length;
    }

    /**
     * 更新/修改职位信息
     * @param position 职位实体
     * @return int
     */
    public int updatePosById(Position position) {
        return positionMapper.updatePosById(position);
    }

}
