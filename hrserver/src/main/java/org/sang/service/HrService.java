package org.sang.service;

import org.sang.bean.Hr;
import org.sang.common.HrUtils;
import org.sang.mapper.HrMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by sang on 2017/12/28.
 */
@Service
@Transactional
public class HrService implements UserDetailsService {

    @Autowired
    HrMapper hrMapper;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        Hr hr = hrMapper.loadUserByUsername(s);
        if (hr == null) {
            throw new UsernameNotFoundException("用户名不对");
        }
        return hr;
    }

    /**
     * HR/管理员注册
     *
     * @param username 用户名
     * @param password 密码
     * @return
     */
    public int hrReg(String username, String password) {
        //如果用户名存在，返回错误
        if (hrMapper.loadUserByUsername(username) != null) {
            return -1;
        }
        BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
        String encode = encoder.encode(password);
        return hrMapper.hrReg(username, encode);
    }

    /**
     * 根据搜索关键字查找HR
     *
     * @param keywords 关键字
     * @return
     */
    public List<Hr> getHrsByKeywords(String keywords) {
        return hrMapper.getHrsByKeywords(keywords);
    }

    /**
     * 更新Hr的信息
     *
     * @param hr hr实体
     * @return
     */
    public int updateHr(Hr hr) {
        return hrMapper.updateHr(hr);
    }

    /**
     * 更新/修改HR的角色信息
     *
     * @param hrId HRid
     * @param rids 角色id(多个角色用","拼接)
     * @return
     */
    public int updateHrRoles(Long hrId, Long[] rids) {
        int i = hrMapper.deleteRoleByHrId(hrId);
        return hrMapper.addRolesForHr(hrId, rids);
    }

    /**
     * 根据HRid获取Hr的信息
     *
     * @param hrId HRid
     * @return
     */
    public Hr getHrById(Long hrId) {
        return hrMapper.getHrById(hrId);
    }

    /**
     * 根据HRid删除HR
     *
     * @param hrId Hrid
     * @return
     */
    public int deleteHr(Long hrId) {
        return hrMapper.deleteHr(hrId);
    }
    /**
     * 获取所有的HR信息(不包括管理员)
     * @return
     */
    public List<Hr> getAllHrExceptAdmin() {
        return hrMapper.getAllHr(HrUtils.getCurrentHr().getId());
    }

    public List<Hr> getAllHr() {
        return hrMapper.getAllHr(null);
    }
}
